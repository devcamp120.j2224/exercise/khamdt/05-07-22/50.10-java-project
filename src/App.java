public class App {
    public static void main(String[] args) throws Exception {
        /* comment nhiều dòng (khối)
         * 
         */
        
        System.out.println("Hello, World!");
        String strName = new String("Devcamp");
        System.out.println(strName); // comment 1 dòng in biến StrName ra console
        /**
         * ví dụ phương thức của lớp string
         */
        System.out.println("Chuyển về chữ thường: ToLowerCase: " + strName.toLowerCase());
        System.out.println("Chuyển về chữ hoa: ToUpperCase: " + strName.toUpperCase());
        System.out.println("Chiều dài của chuỗi: length " + strName.length());
    
    }
}
